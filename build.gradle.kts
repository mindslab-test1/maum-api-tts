import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import com.google.protobuf.gradle.*

buildscript {
	extra["springBootVersion"] = "2.3.3.RELEASE"
	extra["kotlinVersion"] = "1.4.10"

	repositories {
		mavenCentral()
	}

	dependencies {
		classpath("com.google.protobuf:protobuf-gradle-plugin:0.8.10")
		classpath("org.springframework.boot:spring-boot-gradle-plugin:${project.extra["springBootVersion"]}")
		classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:${project.extra["kotlinVersion"]}")
		classpath("org.jetbrains.kotlin:kotlin-allopen:${project.extra["kotlinVersion"]}")
	}
}

plugins {
	id("org.springframework.boot") version "2.3.3.RELEASE"
	id("io.spring.dependency-management") version "1.0.10.RELEASE"
	id("com.google.protobuf") version "0.8.8"
	kotlin("jvm") version "1.4.10"
	kotlin("plugin.spring") version "1.4.10"
	kotlin("plugin.jpa") version "1.4.10"
	kotlin("plugin.serialization") version "1.4.10"

	// It is a necessary plug-in for an optimized Docker image configuration.
	// Reference Link: https://github.com/palantir/gradle-docker
	id("com.palantir.docker") version "0.22.1"
}

group = "ai.maum.tts"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

val grpcVersion = "1.30.0"
val protocVersion = "3.13.0"

repositories {
	mavenCentral()
	maven {
		url = uri("https://aics-nexus.maum.ai/repository/api-2-snapshots")
		credentials {
			username = "admin"
			password = "Mindslab!1"
		}
	}
}

sourceSets {
	main {
		java {
			srcDir("src/main/protoGen")
		}
	}
}

extra["springCloudVersion"] = "Hoxton.SR8"

dependencies {
	// spring
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("org.springframework.boot:spring-boot-starter-aop")
	implementation("org.springframework.boot:spring-boot-starter-amqp")
	implementation("org.springframework.security.oauth.boot:spring-security-oauth2-autoconfigure:2.1.4.RELEASE")
	implementation("org.springframework.boot:spring-boot-starter-oauth2-resource-server")
	implementation("org.springframework.boot:spring-boot-starter-webflux")

	// grpc
	implementation("io.grpc:grpc-services")
	implementation("io.grpc:grpc-netty")
	implementation("io.github.lognet:grpc-spring-boot-starter:3.5.5")

	// spring third party
	developmentOnly("org.springframework.boot:spring-boot-devtools")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

	// language
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

	// kotlin serialization
	implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.0.0")

	// test
	testImplementation("org.springframework.boot:spring-boot-starter-test") {
		exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
	}

	// API 2
	implementation("ai.maum.lib:lib-auth:0.0.1-SNAPSHOT")
}

protobuf {
	protoc { artifact = "com.google.protobuf:protoc:$protocVersion" }
	plugins {
		id("grpc") { artifact = "io.grpc:protoc-gen-grpc-java:$grpcVersion" }
	}
	generateProtoTasks {
		ofSourceSet("main").forEach { task ->
			//            task.builtins {
//                id("java") {
//                    outputSubDir = "protoGen"
//                }
//            }
			task.plugins {
				id("grpc") {
					outputSubDir = "protoGen"
				}
			}
		}
	}
	generatedFilesBaseDir = "$projectDir/src/"
}

dependencyManagement {
	imports {
		mavenBom("org.springframework.cloud:spring-cloud-dependencies:${property("springCloudVersion")}")
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}

tasks.create<Copy>("unpack") {
	dependsOn(tasks.bootJar)
	from(zipTree(tasks.bootJar.get().outputs.files.singleFile))
	into("build/dependency")
}

docker {
	name = "maum-api-tts"
	copySpec.from(tasks.findByName("unpack")?.outputs).into("dependency")
	buildArgs(mapOf("DEPENDENCY" to "dependency"))
}

