import json
import time
import pika


SPRING_PROFILE="local"
MESSAGE_CREATE = {
    "action": "create",
    "modelName": "created_model",
    "modelInfo": {
        "ip": "127.0.0.1",
        "port": 9999,
        "sampleRate": 22050,
        "speakerId": 0,
        "open": False
    },
    "clientAction": "add",
    "client": "creator1"
}

MESSAGE_UPDATE_ADD = {
    "action": "update",
    "modelName": "created_model",
    "modelInfo": {
        "ip": "ip",
        "port": 9997,
        "sampleRate": 22050,
        "speakerId": 0,
        "open": True
    },
    "clientAction": "add",
    "client": "user1"
}

MESSAGE_UPDATE_REMOVE = {
    "action": "update",
    "modelName": "created_model",
    "modelInfo": None,
    "clientAction": "remove",
    "client": "user1"
}

MESSAGE_DELETE = {
    "action": "delete",
    "modelName": "created_model",
    "modelInfo": None,
    "clientAction": "none",
    "client": ""
}

MESSAGE_ERROR_WRONG_TYPE = "hi"


def send_message(channel, exchange, body):
    channel.basic_publish(exchange=exchange, routing_key="", body=body)
    print("sent message: {}".format(body))


if __name__ == '__main__':
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()
    exchange = 'tts-model-update-{}'.format(SPRING_PROFILE)

    channel.exchange_declare(exchange=exchange, durable=True, exchange_type='fanout')

    # try:
    #     while True:
    #         send_message(channel=channel, exchange=exchange, body="hello")
    #         time.sleep(2)
    # except KeyboardInterrupt:
    #     print("Interrupted")
    #     connection.close()
    #     sys.exit(0)

    send_message(channel=channel, exchange=exchange, body=json.dumps(MESSAGE_CREATE))
    send_message(channel=channel, exchange=exchange, body=json.dumps(MESSAGE_UPDATE_ADD))
    send_message(channel=channel, exchange=exchange, body=json.dumps(MESSAGE_UPDATE_REMOVE))
    send_message(channel=channel, exchange=exchange, body=json.dumps(MESSAGE_DELETE))
    send_message(channel=channel, exchange=exchange, body=MESSAGE_ERROR_WRONG_TYPE)
