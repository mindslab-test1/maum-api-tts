package ai.maum.tts.core.model

import kotlinx.serialization.Serializable

@Serializable
data class TtsModel(
        val host: String,
        val port: Int,
        val sampleRate: Int? = 22050,
        val speakerId: Int? = 0,
        val lang: String? = "ko_KR",
        val open: Boolean? = false
)