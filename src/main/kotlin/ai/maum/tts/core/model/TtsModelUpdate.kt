package ai.maum.tts.core.model

import kotlinx.serialization.Serializable

@Serializable
data class TtsModelUpdate(
        val action: String? = "none",
        val modelName: String? = "",
        val modelInfo: TtsModel? = null,
        val clientAction: String? = "none",
        val client: String? = null
)