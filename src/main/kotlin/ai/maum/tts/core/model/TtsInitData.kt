package ai.maum.tts.core.model

import kotlinx.serialization.Serializable

@Serializable
data class TtsAdminInitResponse(
        val status: Int? = 0,
        val message: String? = "",
        val payload: TtsInitData = TtsInitData()
)

@Serializable
data class TtsInitData(
        val modelList: List<TtsModelWithUsers> = listOf()
)

@Serializable
data class TtsModelWithUsers(
        val model: TtsModel,
        val modelName: String,
        val users: Set<String>
)