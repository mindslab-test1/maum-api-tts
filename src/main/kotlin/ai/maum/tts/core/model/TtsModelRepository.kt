package ai.maum.tts.core.model

interface TtsModelRepository {
    fun getModelInfo(clientId: String, model: String): TtsModel
    fun update(ttsModelUpdate: TtsModelUpdate)
    fun testGetModels(): MutableMap<String, Pair<TtsModel, MutableSet<String>>>
}