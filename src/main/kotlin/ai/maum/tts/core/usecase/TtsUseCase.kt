package ai.maum.tts.core.usecase

import ai.maum.tts.core.model.TtsModel
import ai.maum.tts.core.model.TtsModelRepository
import ai.maum.tts.core.service.TtsService
import ai.maum.tts.infra.aop.PostSaveContents
import ai.maum.tts.utils.AudioFormat
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException

@Component
class TtsUseCase (
        private val ttsService: TtsService,
        private val ttsModelRepository: TtsModelRepository
){
    private val logger = LoggerFactory.getLogger(this.javaClass)

    @PostSaveContents
    fun ttsStream(clientId: String, text: String, model: String, audioFormatStr: String = "wav"): Any {
        val audioFormat = try {
            AudioFormat.valueOf(audioFormatStr.toUpperCase())
        } catch (e: IllegalArgumentException){
            logger.warn("illegal argument presented: $audioFormatStr")
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "audio format $audioFormatStr is not supported")
        }
        val modelInfo = ttsModelRepository.getModelInfo(clientId = clientId, model = model)
        return ttsService.ttsStream(ttsModel = modelInfo, modelName = model, text = text, audioFormat = audioFormat)
    }

    fun testGetMemoryModels(): MutableMap<String, Pair<TtsModel, MutableSet<String>>>{
        return ttsModelRepository.testGetModels()
    }
}