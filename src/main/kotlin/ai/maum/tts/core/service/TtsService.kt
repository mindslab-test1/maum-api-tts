package ai.maum.tts.core.service

import ai.maum.tts.core.model.TtsModel
import ai.maum.tts.utils.AudioFormat

interface TtsService{
    fun ttsStream(ttsModel: TtsModel, modelName: String, text: String, audioFormat: AudioFormat = AudioFormat.WAV): Any
    fun ttsMakeFile(ttsModel: TtsModel, text: String): Any
    fun ttsGetFile(ttsModel: TtsModel, text: String): Any
}