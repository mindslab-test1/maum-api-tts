package ai.maum.tts.core.exception

open class ApiException (
        val payload: Any? = null,
        override val message: String?
): RuntimeException(message)

class ApiInternalException(
        payload: Any? = null,
        override val message: String?
): ApiException(payload, message)

class ApiTransactionalException(
        payload: Any? = null,
        override val message: String?
): ApiException(payload, message)

class ApiProcessException(
        payload: Any? = null,
        override val message: String?
): ApiException(payload, message)