package ai.maum.tts.core.exception

abstract class MqException(
        val mqMessage: String?,
        val mqObject: Any?,
        override val message: String?
): RuntimeException(message)

class MqMessageParseException(
        mqMessage: String? = null,
        mqObject: Any? = null,
        override val message: String?
): MqException(mqMessage, mqObject, message)

class MqUnknownActionException(
        mqMessage: String? = null,
        mqObject: Any? = null,
        override val message: String?
): MqException(mqMessage, mqObject, message)

class MqInsufficientDataException(
        mqMessage: String? = null,
        mqObject: Any? = null,
        override val message: String?
): MqException(mqMessage, mqObject, message)

class MqConCurrencyException(
        mqMessage: String? = null,
        mqObject: Any? = null,
        override val message: String?
): MqException(mqMessage, mqObject, message)
