package ai.maum.tts

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TtsApplication

fun main(args: Array<String>) {
	runApplication<TtsApplication>(*args)
}
