package ai.maum.tts.infra.service

import ai.maum.tts.core.exception.*
import ai.maum.tts.core.model.*
import ai.maum.tts.utils.handler.TtsChannelHandler
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.server.ResponseStatusException

@Service
class TtsModelRepositoryImpl(
        ttsAdminWebClient: WebClient,
        val ttsChannelHandler: TtsChannelHandler
): TtsModelRepository{
    private val logger = LoggerFactory.getLogger(this.javaClass)

    private val memoryMap = mutableMapOf<String, Pair<TtsModel, MutableSet<String>>>()

    init {
        val initRequest = ttsAdminWebClient
                .get()
                .uri("/admin/api/init/getInitialData")
                .header("ai-maum-engine", "TTS")
                .retrieve()

        val responseRaw = initRequest.bodyToMono(String::class.java)
                .block() ?: throw ApiTransactionalException(
                message = "response from admin is null!"
        )
        logger.debug("${Json.decodeFromString<TtsAdminInitResponse>(responseRaw)}")

        val responsePayload = Json.decodeFromString<TtsAdminInitResponse>(responseRaw).payload
        responsePayload.modelList.forEach { modelWithUsers ->
            val modelName = modelWithUsers.modelName
            val modelInfo = TtsModel(
                    host = modelWithUsers.model.host,
                    port = modelWithUsers.model.port,
                    sampleRate = modelWithUsers.model.sampleRate,
                    speakerId = modelWithUsers.model.speakerId,
                    open = modelWithUsers.model.open,
                    lang = modelWithUsers.model.lang
            )
            val clientSet = modelWithUsers.users as MutableSet<String>
            memoryMap[modelName] = Pair(first = modelInfo, second = clientSet)
        }

        logger.info("loaded model list from admin server")
        for (mutableEntry in memoryMap) {
            logger.debug(mutableEntry.key)
            logger.debug(mutableEntry.value.toString())
        }
    }

    override fun getModelInfo(clientId: String, model: String): TtsModel {
        val modelPair = memoryMap[model] ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "requested model is not present")
        if(!(modelPair.first.open == true || modelPair.second.contains(clientId)))
            throw  ResponseStatusException(HttpStatus.FORBIDDEN, "model not authenticated for client")

        return modelPair.first
    }

    override fun update(ttsModelUpdate: TtsModelUpdate) {
        when(ttsModelUpdate.action){
            "create" -> {
                createAction(ttsModelUpdate)
            }
            "update" -> {
                updateAction(ttsModelUpdate)
            }
            "delete" -> {
                val removed = memoryMap.remove(ttsModelUpdate.modelName)
                logger.info("removed model: ${ttsModelUpdate.modelName}")
                logger.debug("model info: $removed")
                ttsChannelHandler.removeChannel(ttsModelUpdate.modelName)
            }
            "none" -> {
                logger.warn("no action message presented")
            }
            else -> throw MqUnknownActionException(
                    message = "unknown action ${ttsModelUpdate.action}",
                    mqObject = ttsModelUpdate
            )
        }
    }

    override fun testGetModels(): MutableMap<String, Pair<TtsModel, MutableSet<String>>> {
        return this.memoryMap
    }

    fun createAction(ttsModelUpdate: TtsModelUpdate){
        ttsModelUpdate.modelInfo ?: throw MqInsufficientDataException(
                message = "model info not specified in message",
                mqObject = ttsModelUpdate
        )
        ttsModelUpdate.client ?: throw MqInsufficientDataException(
                message = "model creator(default user) not specified in message",
                mqObject = ttsModelUpdate
        )

        val pair = Pair(
                first = ttsModelUpdate.modelInfo,
                second = mutableSetOf(ttsModelUpdate.client)
        )
        memoryMap[ttsModelUpdate.modelName ?: throw MqConCurrencyException(
                message = "null model name presented"
        )] = pair
        logger.info("created model: ${ttsModelUpdate.modelName}")
        logger.debug("model info: ${ttsModelUpdate.modelInfo}")

    }

    fun updateAction(ttsModelUpdate: TtsModelUpdate){
        memoryMap[ttsModelUpdate.modelName] ?: throw MqConCurrencyException(
                message = "concurrency exception: no known model name '${ttsModelUpdate.modelName}', " +
                        "known models: ${memoryMap.keys}"
        )

        val clientSet: MutableSet<String> = LinkedHashSet()
        clientSet.addAll(memoryMap[ttsModelUpdate.modelName]?.second ?: throw ApiInternalException(
                message = "could not find client set for model: ${ttsModelUpdate.modelName}"
        ))
        val originModel = memoryMap[ttsModelUpdate.modelName]?.first ?: throw ApiInternalException(
                message = "could not find origin model info: ${ttsModelUpdate.modelName}"
        )

        when (ttsModelUpdate.clientAction){
            "add" -> clientSet.add(ttsModelUpdate.client ?: throw MqInsufficientDataException(
                    message = "client info not specified in message"
            ))
            "remove" -> clientSet.remove(ttsModelUpdate.client ?: throw MqInsufficientDataException(
                    message = "client info not specified in message"
            ))
        }

        memoryMap[ttsModelUpdate.modelName ?: throw MqConCurrencyException(
                message = "concurrency exception: no known model name '${ttsModelUpdate.modelName}', " +
                        "known models: ${memoryMap.keys}"
        )] = Pair(
                first = ttsModelUpdate.modelInfo ?: originModel,
                second = clientSet
        )

        logger.info("updated model: ${ttsModelUpdate.modelName}")
        logger.debug("model info: ${ttsModelUpdate.modelInfo}")
        logger.debug("client action: ${ttsModelUpdate.clientAction}, client: ${ttsModelUpdate.client}")

        ttsChannelHandler.removeChannel(ttsModelUpdate.modelName)
    }
}