package ai.maum.tts.infra.service

import ai.maum.tts.core.exception.MqMessageParseException
import ai.maum.tts.core.model.TtsModelRepository
import ai.maum.tts.core.model.TtsModelUpdate
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.slf4j.LoggerFactory
import org.springframework.amqp.rabbit.annotation.RabbitHandler
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.stereotype.Service

@Service
@RabbitListener(queues = ["#{autoGenQueue.name}"])
class FanoutMessageReceiver(
        val ttsModelRepository: TtsModelRepository
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    @RabbitHandler
    fun receiveMessage(message: String){
        logger.debug("received: $message")
        val modelUpdate: TtsModelUpdate = try {
            Json.decodeFromString(message)
        } catch (e: Exception){
            logger.error(e.toString())
            throw MqMessageParseException(message = e.message, mqMessage = message)
        }
        logger.info(modelUpdate.toString())
        ttsModelRepository.update(modelUpdate)
    }
}