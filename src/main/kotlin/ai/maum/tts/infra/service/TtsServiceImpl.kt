package ai.maum.tts.infra.service

import ai.maum.tts.core.exception.ApiInternalException
import ai.maum.tts.core.exception.ApiProcessException
import ai.maum.tts.core.model.TtsModel
import ai.maum.tts.core.service.TtsService
import ai.maum.tts.utils.AudioFormat
import ai.maum.tts.utils.handler.TtsChannelHandler
import maum.brain.tts.NgTtsServiceGrpc
import maum.brain.tts.TtsRequest
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.util.concurrent.TimeUnit

@Service
class TtsServiceImpl(
        val ttsChannelHandler: TtsChannelHandler
) : TtsService {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun ttsStream(ttsModel: TtsModel, modelName: String, text: String, audioFormat: AudioFormat): Any {
        val grpcChannel = ttsChannelHandler.getOrPutChannel(
                modelName = modelName, modelInfo = ttsModel
        )

        try {
            val stub = NgTtsServiceGrpc.newBlockingStub(grpcChannel)
            return stub.withDeadlineAfter(30, TimeUnit.SECONDS).speakWav(
                    TtsRequest.newBuilder()
                            .setLangValue(this.getLangVal(ttsModel.lang!!))
                            .setSampleRate(ttsModel.sampleRate!!)
                            .setText(text)
                            .setSpeaker(ttsModel.speakerId!!)
                            .setAudioEncodingValue(this.getAudioEnumVal(audioFormat = audioFormat))
                            .build()
            )
        } catch (e: Exception) {
            logger.error("exception handling response: $e")
            throw ApiProcessException(
                    message = e.message
            )
        }
    }

    override fun ttsMakeFile(ttsModel: TtsModel, text: String): Any {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ttsGetFile(ttsModel: TtsModel, text: String): Any {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun getLangVal(lang: String): Int{
        return when(lang) {
            "0", "ko_KR", "kor", "ko" -> 0
            "1", "en_US", "eng", "en" -> 1
            else -> throw ApiInternalException(
                    message = "unknown lang type in model info!"
            )
        }
    }

    private fun getAudioEnumVal(audioFormat: AudioFormat): Int{
        return when(audioFormat){
            AudioFormat.WAV -> 0
            AudioFormat.PCM -> 1
        }
    }
}