package ai.maum.tts.infra.aop

import ai.maum.tts.core.service.MediaService
import org.aspectj.lang.JoinPoint
import org.aspectj.lang.annotation.AfterReturning
import org.aspectj.lang.annotation.Aspect
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import org.springframework.web.multipart.MultipartFile


@Component
@Aspect
class SaveContentsAspect(
    private val mediaService: MediaService
) {

    private val logger = LoggerFactory.getLogger(this.javaClass)

    @AfterReturning(
            pointcut =
                "@annotation(ai.maum.tts.infra.aop.PostSaveContents) && " +
                "execution(public * *(..)) && " +
                "args(file)",
            returning = "result")
    fun saveContents(joinPoint: JoinPoint, result: String, file: MultipartFile) {
        logger.info("executing the @AfterReturning on method: ${joinPoint.signature.toShortString()}")
        logger.info("request param(${file.originalFilename})")
        logger.info("return value($result)")

        if (result.isBlank()) return

        // call async for grpc
        mediaService.saveContent(file)
    }
}