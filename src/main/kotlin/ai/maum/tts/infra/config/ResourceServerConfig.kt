package ai.maum.tts.infra.config

import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer
import org.springframework.security.oauth2.jwt.JwtDecoder
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore
import java.security.KeyFactory
import java.security.interfaces.RSAPublicKey
import java.security.spec.X509EncodedKeySpec
import java.util.*

@Configuration
@EnableResourceServer
class ResourceServerConfig(
    private val jwtSignKeyConfig: JwtSignKeyConfig
) : ResourceServerConfigurerAdapter() {

    override fun configure(resources: ResourceServerSecurityConfigurer) {
        resources
                .tokenStore(tokenStore())
                .resourceId(null) // Disabled since Microsoft does not support resource ids.
    }

    override fun configure(http: HttpSecurity) {
        http.headers().frameOptions().disable()
        http.authorizeRequests()
                .antMatchers("/tts/**")
                    .access(
                            "(hasAuthority('ROLE_API') || hasAuthority('ROLE_API_ADMIN')) && " +
                                    "(#oauth2.hasScope('ai-maum-api-use-*') || " +
                                    "#oauth2.hasScope('ai-maum-api-use-tts') || " +
                                    "#oauth2.hasScope('ai-maum-api-admin-*'))"
                    )
                .anyRequest().authenticated()
    }

    @Bean
    fun tokenStore(): JwtTokenStore {
        return JwtTokenStore(jwtAccessTokenConverter())
    }

    @Bean
    fun jwtAccessTokenConverter(): JwtAccessTokenConverter {
        val accessTokenConverter = JwtAccessTokenConverter()
        accessTokenConverter.setVerifierKey(jwtSignKeyConfig.publicKey)
        return accessTokenConverter
    }

    @Bean
    fun jwtDecoder(properties: OAuth2ResourceServerProperties): JwtDecoder {
        val jwtPublicKey = jwtSignKeyConfig.publicKey
        val publicKey = jwtPublicKey.replace("-----BEGIN PUBLIC KEY-----", "")
                .replace("-----END PUBLIC KEY-----", "")
                .replace("\\s+".toRegex(), "")
        val x509EncodedKeySpec = X509EncodedKeySpec(Base64.getDecoder().decode(publicKey))

        val rsaPublicKey = KeyFactory.getInstance("RSA").generatePublic(x509EncodedKeySpec)
        return NimbusJwtDecoder.withPublicKey(rsaPublicKey as RSAPublicKey?).build()
    }
}