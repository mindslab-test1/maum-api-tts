package ai.maum.tts.infra.config

import ai.maum.lib.auth.MaumAiAuthRequest
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.client.WebClient

@Configuration
class HttpConnectionConfig(
        @Value("\${maum-api.admin.address}")
        val hostUrl: String,
        @Value("\${maum-api.auth.clientId}")
        val adminClientId: String,
        @Value("\${maum-api.auth.clientKey}")
        val adminClientKey: String,
        @Value("\${maum-api.auth.address}")
        val authAddress: String,
) {
    @Bean
    fun ttsAdminWebClient(): WebClient{
        val auth = MaumAiAuthRequest
        auth.authUrl = "http://$authAddress/auth"
        auth.setCredentials(
                adminClientId,
                adminClientKey
        )
        val clientToken = auth.getAccessToken()

        return WebClient.builder()
                .baseUrl("http://$hostUrl")
                .defaultHeader(
                        "Authorization",
                        "Bearer ${clientToken.accessToken}"
                )
                .build()
    }
}