package ai.maum.tts.utils

enum class AudioFormat(val value: String) {
    PCM("pcm"),
    WAV("wav");
}