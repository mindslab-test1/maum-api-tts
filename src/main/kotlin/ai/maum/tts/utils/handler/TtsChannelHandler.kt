package ai.maum.tts.utils.handler

import ai.maum.tts.core.model.TtsModel
import io.grpc.ManagedChannel

interface TtsChannelHandler {
    fun getOrPutChannel(modelName: String, modelInfo: TtsModel): ManagedChannel
    fun removeChannel(modelName: String?): Unit
}