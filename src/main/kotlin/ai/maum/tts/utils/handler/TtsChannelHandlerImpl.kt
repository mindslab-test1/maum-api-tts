package ai.maum.tts.utils.handler

import ai.maum.tts.core.model.TtsModel
import io.grpc.ManagedChannel
import io.grpc.ManagedChannelBuilder
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import javax.annotation.PreDestroy

@Component
class TtsChannelHandlerImpl: TtsChannelHandler {
    // modelName, (GRPC channel, lastReferencedMillis)
    val channelMap: MutableMap<String, ChannelHolder> = mutableMapOf()
    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun getOrPutChannel(modelName: String, modelInfo: TtsModel): ManagedChannel {
        logger.debug("get or put channel: $modelName")
        logger.debug("model address info: $modelInfo")
        try{
            val returnChannel: ManagedChannel

            when(channelMap.contains(modelName)){
                true -> {
                    val channelHolder = channelMap[modelName]
                    channelHolder?.lastReferencedEpoch = System.currentTimeMillis()
                    logger.debug("channel holder referenced updated: ${channelHolder?.lastReferencedEpoch}")
                    returnChannel = channelHolder?.channel ?: ManagedChannelBuilder.forAddress(modelInfo.host, modelInfo.port).usePlaintext().build()
                    logger.debug("in map holder referenced check: ${channelMap[modelName]?.lastReferencedEpoch}")
                }
                else -> {
                    logger.debug("add new channel")
                    val channelHolder = ChannelHolder(
                            ManagedChannelBuilder.forAddress(modelInfo.host, modelInfo.port).usePlaintext().build(),
                            System.currentTimeMillis()
                    )
                    channelMap[modelName] = channelHolder
                    returnChannel = channelHolder.channel
                }
            }

            return returnChannel
        } finally {
            manageChannel()
        }
    }

    override fun removeChannel(modelName: String?) {
        logger.debug("remove channel: $modelName")
        channelMap.remove(modelName)?.channel?.shutdown()
    }

    private fun manageChannel(){
        if(channelMap.size >= 10){
            logger.debug("maintained channel over 10")
            channelMap.popByMin(
                    Comparator{ a: ChannelHolder, b: ChannelHolder ->
                        return@Comparator (a.lastReferencedEpoch - b.lastReferencedEpoch).toInt()
                    }
            )?.channel?.shutdown()
            logger.debug("shutdown most early referenced")
        }
    }

    @PreDestroy
    fun onDestroy(){
        for (mutableEntry in channelMap){
            logger.debug("shutdown all channels")
            mutableEntry.value.channel.shutdown()
        }
    }
}

fun <K, V> MutableMap<K, V>.popByMin(comparator: Comparator<V>): V?{
    return if(this.isNotEmpty()){
        var minEntry: Map.Entry<K, V>? = null
        forEach {
            if(minEntry == null) minEntry = it
            else{
                if(comparator.compare(it.value, minEntry!!.value) < 0) minEntry = it
            }
        }

        this.remove(minEntry?.key)
    } else null
}

data class ChannelHolder(
        val channel: ManagedChannel,
        var lastReferencedEpoch: Long = 0
)