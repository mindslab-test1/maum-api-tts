package ai.maum.tts.boundaries.dto

import kotlinx.serialization.Serializable

@Serializable
data class TtsRequestDto (
        val model: String,
        val text: String,
        val audioFormat: String = "wav"
)