package ai.maum.tts.boundaries.dto

import ai.maum.tts.core.model.TtsModel
import kotlinx.serialization.Serializable

@Serializable
data class TestResponseDto (
        val memoryMapStatus: MutableMap<String, Pair<TtsModel, MutableSet<String>>>
)