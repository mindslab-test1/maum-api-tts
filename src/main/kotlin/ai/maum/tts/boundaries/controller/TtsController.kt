package ai.maum.tts.boundaries.controller

import ai.maum.stats.protobuf.SaveStatisticsRequest
import ai.maum.stats.protobuf.StatsGrpcServiceGrpc
import ai.maum.tts.boundaries.dto.TtsRequestDto
import ai.maum.tts.core.exception.ApiTransactionalException
import ai.maum.tts.core.model.TtsModel
//import ai.maum.tts.core.model.TtsRequestInfo
import ai.maum.tts.core.usecase.TtsUseCase
import io.grpc.ManagedChannel
import io.grpc.ManagedChannelBuilder
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonElement
import maum.brain.tts.TtsMediaResponse
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.oauth2.jwt.JwtDecoder
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody
import java.nio.channels.Channels
import java.util.concurrent.Callable
import javax.annotation.PostConstruct
import javax.servlet.http.HttpServletResponse

@RestController
@RequestMapping("/tts")
class TtsController (
        val ttsUseCase: TtsUseCase,
        val jwtDecoder: JwtDecoder
//        @Value("\${maum.stats.address}")
//        val statsTarget: String
){
    private val logger = LoggerFactory.getLogger(this.javaClass)

    @PostMapping("/stream")
    fun ttsStream(
            @RequestHeader httpHeaders: HttpHeaders,
            @RequestBody ttsRequestDto: TtsRequestDto,
            httpServletResponse: HttpServletResponse,
            oAuth2Authentication: OAuth2Authentication): Any{
        logger.debug("tts stream")
        logger.debug(httpHeaders.getFirst(HttpHeaders.AUTHORIZATION))
        logger.debug(httpHeaders.getFirst("Request-Unique-Key"))

        val tokenValue = (oAuth2Authentication.details as OAuth2AuthenticationDetails).tokenValue
        val jwtMap = jwtDecoder.decode(tokenValue)
        val clientId = jwtMap.getClaim<String>("app_id")
        val requestId = httpHeaders.getFirst("Request-Unique-Key")
        logger.debug(clientId)

//        try{
//            val grpcChannel = ManagedChannelBuilder.forTarget(statsTarget).usePlaintext().build()
//            val stub = StatsGrpcServiceGrpc.newBlockingStub(grpcChannel)
//            val response = stub.saveStatistics(
//                    SaveStatisticsRequest.newBuilder()
//                            .setCollection("tts_test")
//                            .setPayload(
//                                    Json.encodeToString(
//                                            TtsRequestInfo(
//                                                    requestId = requestId,
//                                                    clientId = clientId,
//                                                    requestBody = ttsRequestDto
//                                            )
//                                    )
//                            )
//                            .build()
//            )
//
//            logger.info(response.message)
//        } catch (e: Exception){
//            logger.error(e.message)
//        }
        val iterator = ttsUseCase.ttsStream(
                clientId = clientId,
                text = ttsRequestDto.text,
                model = ttsRequestDto.model,
                audioFormatStr = ttsRequestDto.audioFormat
        )
        val outChannel = Channels.newChannel(httpServletResponse.outputStream)
        try {
            while ((iterator as Iterator<*>).hasNext()){
                val data = (iterator.next() as TtsMediaResponse).mediaData
                val buff = data.asReadOnlyByteBuffer()
                outChannel.write(buff)
            }
            httpServletResponse.contentType = "audio/x-wav;charset=UTF-8"
        } catch (e: Exception){
            httpServletResponse.status = 500
            logger.error("exception while iterating response: ${e.message}")
            throw ApiTransactionalException(message = "exception while writing to output stream")
        } finally {
            outChannel.close()
            httpServletResponse.outputStream.close()
        }
        return Unit
    }

    @GetMapping("/test/getModelList")
    fun getModelList(@RequestHeader httpHeaders: HttpHeaders): ResponseEntity<MutableMap<String, Pair<TtsModel, MutableSet<String>>>> {
        return ResponseEntity.ok(ttsUseCase.testGetMemoryModels())
    }
}