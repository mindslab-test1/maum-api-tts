package ai.maum.tts.boundaries.handler

import ai.maum.tts.core.exception.ApiException
import ai.maum.tts.core.exception.ApiInternalException
import ai.maum.tts.core.exception.ApiProcessException
import ai.maum.tts.core.exception.ApiTransactionalException
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.BindException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class ApiExceptionHandler: ResponseEntityExceptionHandler() {
    @ExceptionHandler(ApiException::class)
    fun handleApiException(exception: ApiException, request: WebRequest): ResponseEntity<Any?>? {
        logger.warn("handle api exception")

        val code = when(exception){
            is ApiInternalException -> {
                logger.error("Internal exception")
                logger.error(exception.message)
                500
            }
            is ApiTransactionalException -> {
                logger.error("transactional exception")
                logger.error(exception.message)
                500
            }
            is ApiProcessException -> {
                logger.error("process exception")
                logger.error(exception.message)
                500
            }
            else -> {
                logger.error("unknown exception")
                logger.error(exception.message)
                500
            }
        }

        val responseBody = mapOf(
                "code" to code,
                "message" to "internal error has occured."
        )

        val status = HttpStatus.resolve(code) ?: run {
            logger.error("could not find status code")
            return@run HttpStatus.INTERNAL_SERVER_ERROR
        }

        return handleExceptionInternal(
                exception,
                responseBody,
                HttpHeaders(),
                status,
                request
        )
    }

    override fun handleBindException(
            ex: BindException,
            headers: HttpHeaders,
            status: HttpStatus,
            request: WebRequest
    ): ResponseEntity<Any> {
        logger.info("handleBindException")
        return super.handleBindException(ex, headers, status, request)
    }
}