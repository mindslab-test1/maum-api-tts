package ai.maum.tts.boundaries.handler

import ai.maum.tts.core.exception.*
import org.springframework.amqp.AmqpRejectAndDontRequeueException
import org.springframework.util.ErrorHandler

class MqExceptionHandler: ErrorHandler {
    override fun handleError(t: Throwable) {
        when (t) {
            is MqException -> {
                when (t){
                    is MqMessageParseException -> throw AmqpRejectAndDontRequeueException("message parse exception")
                    is MqUnknownActionException -> throw AmqpRejectAndDontRequeueException("unknown action exception")
                    is MqInsufficientDataException -> throw AmqpRejectAndDontRequeueException("insufficient data exception")
                    is MqConCurrencyException -> throw AmqpRejectAndDontRequeueException("internal data concurrency exception")
                    else -> throw AmqpRejectAndDontRequeueException("unknown exception?? ${t::class}", t)
                }

            }
            is ApiException -> {
                throw AmqpRejectAndDontRequeueException("")
            }
        }
    }
}