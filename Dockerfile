FROM openjdk:11.0.8-jre-slim
MAINTAINER aquashdw@mindslab.ai

VOLUME /tmp

ARG DEPENDENCY=target/dependency
COPY ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY ${DEPENDENCY}/META-INF /app/META-INF
COPY ${DEPENDENCY}/BOOT-INF/classes /app

# Error: Main method not found in class ai.maum.api.MaumApiGateApplication (or MaumApiGateApplication.kt)
# main method가 정의된 파일이름 or 확장자를 포함한 파일명은 위와 같은 Error가 발생함.
# Resolved: 확장자까지 카멜케이스 형태로 해야함
ENTRYPOINT ["java","-cp","app:app/lib/*","ai.maum.tts.TtsApplicationKt"]