# maum-api-tts
API 2.0 TTS front

## model destination handling
### basic poko for handling 
```kotlin
@Serializable
data class TtsModel(
    val ip: String,
    val port: Int,
    val sampleRate: Int? = 22050,
    val speakerId: Int? = 1,
    val open: Boolean? = false
)
```
keep different TtsModel objects even for same physical models
### client - model availability handling
pair model poko and user set  
use hashmap as (key:String(model)):(value:Pair<TtsModel, MutableSet<String>>)

### voiceName alias
remove voiceName alias from API 1.0 feature (increased administration problems)

## Model Update Message
C(R)UD  
Read action is non-existent
### message poko
```kotlin
@Serializable
data class TtsModelUpdate(
        val action: String,
        val modelName: String,
        val modelInfo: TtsModel,
        val clientAction: String,
        val client: String?
)
```
### message json
```json
{
  "action": "create",
  "modelName": "modelName",
  "modelInfo": {
    "ip": "ip",
    "port": 8080, 
    "sampleRate": 22050,
    "speakerId": 0,
    "open": false
  },
  "clientAction": "add",
  "client": ""
}
``` 
parameter description
```
action: what to do (create, update, delete)  
action.create: insert new model with 'modelInfo'
action.update: update model with 'modelInfo' + client action
action.delete: remove model with 'modelName'
modelName: target model name
modelInfo: json representation of TtsModel
clientAction: what to do with given client (add, remove, none)  
clientAction.add: add 'client' to 'modelName' (for 'action'=='created', should be creator of model)
clientAction.remove: remove 'client' from 'modelName'
clientAction.none: do nothing
client: target client id
```

## Model Update Refresh Strategy
copy below text to https://sequencediagram.org/
```text
title tts model info strategies

actor user
participant tts api server
participant server internal memory
participant external rabbitmq
participant tts admin server
actor tts admin

group event [pod start]
tts api server->external rabbitmq: subscribe
tts api server->tts admin server: (grpc) request initial data
tts admin server->tts api server: (grpc) send initial data with latest tag
tts api server->server internal memory: update memory
end 


group event [on model C(R)UD]
tts admin->tts admin server: make adjustment
tts admin server->external rabbitmq: produce message
note over external rabbitmq: process event [kafka message produced
end 

group event [redis message produced]
tts api server<-external rabbitmq: receive message
tts api server->tts api server: update inner queue
note over tts api server: await self process
tts api server->server internal memory: update memory
end 

user->tts api server: request tts
group event [unexpected data consistency occurrence]
tts api server->tts admin server: request latest tag
tts api server<-tts admin server: get latest info
alt server latest != admin latest
tts api server<-external rabbitmq: get unreceived messages
tts api server->server internal memory: update
tts api server->tts api server: recheck request
alt no match
tts api server->user:400
else match
tts api server->user: do tts
end 
else server latest == admin latest
tts api server->user: 400
end 
end 
```